const fs = require('fs');
const chalk = require('chalk');


const addNote = (title, body) => {
  debugger;
  const notes = loadNotes();
  const duplicatedNotes = notes.filter(node => node.title === title);
  if (!duplicatedNotes.length) {
    notes.push({ title, body });
    saveNotes(notes);
    console.log(chalk.green.inverse("New note added!"));
  } else {
    console.log(chalk.red.inverse("Note taken!"));
  }
};


const removeNote = (title) => {
  const notes = loadNotes();
  const notesToKeep = notes.filter(node => node.title !== title);
  if (notes.length > notesToKeep.length) {
    console.log(chalk.green.inverse("Note removed!"));
    saveNotes(notesToKeep);
  } else {
    console.log(chalk.red.inverse("No note found!"));
  }
};


const listNotes = () => {
  const notes = loadNotes();
  console.log(chalk.inverse("Your notes"));

  notes.forEach(
    note => console.log(note.title)
  );
};


const readNote = title => {
  const notes = loadNotes();
  const note = notes.find(note => note.title === title);

  if (note) {
    console.log(chalk.inverse(note.title));
    console.log(note.body);
  } else {
    console.log(chalk.red.inverse("No note found!"));
  }
};


const saveNotes = notes => {
  fs.writeFileSync('notes.json', JSON.stringify(notes));
};


const loadNotes = () => {
  try {
    const dataBuffer = fs.readFileSync('notes.json');
    return JSON.parse(dataBuffer.toString());
  } catch (error) {
    return new Array();
  }
};


module.exports = {
  addNote,
  removeNote,
  listNotes,
  readNote,
};
