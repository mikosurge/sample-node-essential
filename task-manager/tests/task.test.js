const request = require('supertest');
const app = require('../app');
const Task = require('../models/task');
const {
  sampleUser,
  sampleUser2,
  taskOne,
  taskTwo,
  taskThree,
  setupDatabase
} = require('./fixtures/db');


beforeEach(setupDatabase);


test("Should create task for user", async () => {
  const response = await request(app)
    .post('/tasks')
    .set('Authorization', `Bearer ${sampleUser.tokens[0].token}`)
    .send({
      description: "Sample description",
    })
    .expect(201);

  const task = await Task.findById(response.body._id);
  expect(task).not.toBeNull();
  expect(task.completed).toEqual(false);
});


test("Should fetch user tasks", async () => {
  const response = await request(app)
    .get('/tasks')
    .set('Authorization', `Bearer ${sampleUser.tokens[0].token}`)
    .send()
    .expect(200);

  expect(response.body.length).toEqual(2);
});


test("Should not delete other users' tasks", async () => {
  const response = await request(app)
    .delete(`/tasks/${taskOne._id}`)
    .set('Authorization', `Bearer ${sampleUser2.tokens[0].token}`)
    .send()
    .expect(404);
  const task = await Task.findById(taskOne._id);
  expect(task).not.toBeNull();
});
