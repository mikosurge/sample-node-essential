const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const User = require('../../models/user');
const Task = require('../../models/task');


const sampleUserId = new mongoose.Types.ObjectId();
const sampleUser = {
  _id: sampleUserId,
  name: 'Paragon',
  email: 'paragon@light.com',
  password: 'surge123@',
  tokens: [{
    token: jwt.sign(
      { _id: sampleUserId },
      process.env.JWT_SECRET
    ),
  }]
};


const sampleUserId2 = new mongoose.Types.ObjectId();
const sampleUser2 = {
  _id: sampleUserId2,
  name: 'Nether',
  email: 'nether@springs.com',
  password: 'nether123@',
  tokens: [{
    token: jwt.sign(
      { _id: sampleUserId2 },
      process.env.JWT_SECRET
    ),
  }]
};


const taskOne = {
  _id: new mongoose.Types.ObjectId(),
  description: "First task",
  completed: true,
  owner: sampleUser._id
};


const taskTwo = {
  _id: new mongoose.Types.ObjectId(),
  description: "Second task",
  completed: true,
  owner: sampleUser._id
};


const taskThree = {
  _id: new mongoose.Types.ObjectId(),
  description: "Third task",
  completed: true,
  owner: sampleUser2._id
};


const setupDatabase = async () => {
  await User.deleteMany();
  await Task.deleteMany();
  await new User(sampleUser).save();
  await new User(sampleUser2).save();
  await new Task(taskOne).save();
  await new Task(taskTwo).save();
  await new Task(taskThree).save();
};


module.exports = {
  sampleUser,
  sampleUser2,
  taskOne,
  taskTwo,
  taskThree,
  setupDatabase,
};
